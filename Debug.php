<?php
namespace bdb\component;

class Debug
{
    static public function debugQuery($query, $run = true)
    {
        $GLOBALS['CUSTOM_DEBUG'] = true;
        print_r("<pre><br>#INIT-DEBUG-QUERY<br>");
        $stack = debug_backtrace()[1];
        print_r('    ' . (isset($stack1['class']) ? $stack1['class'] . '->' : '') . '->' . $stack['function'] . '(...) | caller: ');

        if (isset($stack['file'])) {
            print_r($stack['file'] . '[' . $stack['line'] . ']<br>');
        }

        print_r("<br><br>");
        print_r("<br>#rawSql-1<br>");
        print_r($query->createCommand()->rawSql);
        print_r("<br><br>");

        if ($run) {
            $sqlRes = $query->createCommand()->queryAll();

            foreach ($sqlRes as $row) {
                var_dump($row);
                print_r("<br>");
            }
        }

        print_r("<br>#END-DEBUG-QUERY<br></pre>");
        die;
    }

    static public function debugVar($var)
    {
        $GLOBALS['CUSTOM_DEBUG'] = true;
        print_r("<pre><br>#INIT-DEBUG-VAR<br>");
        $stack0 = debug_backtrace()[0];
        $stack1 = debug_backtrace()[1];
        print_r('break: ' . (isset($stack1['class']) ? $stack1['class'] . '->' : '') . $stack1['function'] .
            '(...) ' . $stack0['file'] . '[' . $stack0['line'] . '] <br>caller: ');

        if (isset($stack['file'])) {
            print_r($stack1['file'] . '[' . $stack1['line'] . ']<br>');
        }
        
        print_r("<br><br>");
        var_dump($var);
        print_r("<br>#END-DEBUG-VAR<br><pre>");
        die;
    }
}