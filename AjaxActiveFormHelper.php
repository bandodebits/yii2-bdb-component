<?php
namespace bdb\component;

use kartik\widgets\ActiveForm;
use Yii;
use yii\debug\models\search\Debug;

class AjaxActiveFormHelper
{
    //TODO create appropriate CSS
    const DEFAULT_FORM_LOADER_JS = '
        var _loader = $("#main-loader")

        if (_loader[0] == undefined) {
             _loader = $($.parseHTML(
                 "<div id=\"form-loader\""+
                 "style=\"position: absolute; top:0; left:0; z-index: 100; opacity: 0.0;"+
                 "background: #1D1D25; display: none ; width: 100%; height: 100%\">" +
                 "<div class=\"spinner\"></div></div>"
            ))

            form.append(_loader)
        }

        var showLoader = function () {
            _loader.show()
            _loader.fadeTo(150, 1.0)
        }

        var hideLoader = function () {
            _loader.fadeTo(150, 0, function () {
                $(this).hide()
            })
        }
    ';

    const DEFAULT_FORM_LOADER_EMBEDDED_JS = '
         _loader = $($.parseHTML(
             "<div id=\"form-loader\""+
             "style=\"position: absolute; top:0; left:0; z-index: 100; opacity: 0.0;"+
             "background: #1D1D25; display: none ; width: 100%; height: 100%\">" +
             "<div class=\"spinner\"></div></div>"
        ))

        form.append(_loader)

        var showLoader = function () {
            _loader.show()
            _loader.fadeTo(150, 1.0)
        }

        var hideLoader = function () {
            _loader.fadeTo(150, 0, function () {
                $(this).hide()
            })
        }
    ';

    public static function render($renderer, $model, $viewBasePath, $id, $action, $params = [], $beforeSubmitJS = null, $afterFinishedJS = null,
                                  $formLoaderJS = AjaxActiveFormHelper::DEFAULT_FORM_LOADER_EMBEDDED_JS, $bypassClientErrors = true)
    {
        //prevent assets load twice via ajax
        if (Yii::$app->request->isAjax) {
            Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
            Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
            Yii::$app->assetManager->bundles['dmstr\bootstrap\BootstrapAsset'] = false;
            Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
            Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapPluginAsset'] = false;
        }

        $form = ActiveForm::begin([
            'id' => $id,
            'options' => ['enctype' => 'multipart/form-data'],
            'action' => $action,
            'enableClientValidation' => true,
            'enableAjaxValidation' => false
        ]);

        $afterForm = null;

        $afterFormSetter = function ($body) use (&$afterForm) {
            $afterForm = $body;
        };

        echo $renderer->render(
            empty($viewBasePath) ? $id : "$viewBasePath/$id",
            array_merge(['model' => $model, 'form' => $form, 'afterFormSetter' => &$afterFormSetter], $params));

        ActiveForm::end();
        ?>

        <script>
            //Run on next frame
            window.setTimeout(function () {
                var form = $('#<?=$form->id?>')
                var formElement = form[0]
                var submitting = false

                formElement.beforeSubmitLocal = function () {
                    return true
                }

                formElement.afterFinishedLocal = function () {
                    return true
                }

                <?= $formLoaderJS ?>

                formElement.onsubmit = function () {
                    if (submitting || !formElement.beforeSubmitLocal()) {
                        return false
                    }

                    if (<?= $bypassClientErrors ? 'true' : 'false' ?> || form.find('.has-error').length == 0){
                        var submit = true

                        <?if($beforeSubmitJS != null){
                             echo '{' ;
                                echo $beforeSubmitJS;
                             echo '}' ;
                        }?>

                        if (submit) {
                            submitting = true
                            showLoader()

                            $.ajax({
                                url: form.attr('action'),
                                type: 'POST',
                                data: new FormData(formElement),
                                async: true,
                                processData: false,
                                contentType: false,
                                dataType: 'json',
                                beforeSend: function () {
                                    //Silent
                                },
                                complete: function () {
                                    //Silent
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    submitting = false
                                    hideLoader()
                                    toastr.error("ajax error:" + errorThrown)
                                },
                                success: function (response) {
                                    var extra = response['_extra'];

                                    if (extra.warnings.length > 0) {
                                        $.each(extra.warnings, function (index, value) {
                                            toastr.warning(value)
                                        });
                                    }

                                    if (extra.errors.length > 0) {
                                        $.each(extra.errors, function (index, value) {
                                            toastr.error(value)
                                        });
                                    }

                                    if (extra.exceptions.length > 0) {
                                        $.each(extra.exceptions, function (index, value) {
                                            toastr.error(value)
                                        });
                                    }

                                    if (extra.messages.length > 0) {
                                        $.each(extra.messages, function (index, value) {
                                            toastr.info(value)
                                        });
                                    }

                                    submitting = false

                                    if (extra.ok) {
                                        if (formElement.afterFinishedLocal()) {
                                            <?if($afterFinishedJS != null){
                                                echo '{' ;
                                                echo $afterFinishedJS;
                                                echo '}';
                                             }?>
                                        } else {
                                            hideLoader()
                                        }
                                    } else {
                                        toastr.warning('<?=Yii::t('app','Please, correct errors before submit')?>')
                                        form.yiiActiveForm('updateMessages', response, false)
                                        hideLoader()
                                    }
                                }
                            })
                        }
                    }else{
                        toastr.warning('<?=Yii::t('app','Please, correct errors before submit')?>')
                    }

                    return false
                }
            }, 1)
        </script>
        <?
        if ($afterForm != null) {
            call_user_func($afterForm);
        }

        return $form;
    }
}

