<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace bdb\component;

use Yii;
use yii\data\SqlDataProvider;

class RichSqlDataProvider extends SqlDataProvider
{
    protected function prepareModels()
    {
        $data = parent::prepareModels();
        $out = [];

        foreach ($data as $row) {
            $out[] = new DynamicModel($row);
        }

        return $out;
    }
}
