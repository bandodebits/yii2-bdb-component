<?php
namespace bdb\component\exception;

use yii\web\GoneHttpException;

class ApplicationException extends GoneHttpException
{
    public function __construct($message = 'Application exception', $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}


