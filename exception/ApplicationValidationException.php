<?php
namespace bdb\component\exception;

class ApplicationValidationException extends ApplicationException
{
    public function __construct($message = 'Validation exception', $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}


