<?php
namespace bdb\component\exception;

class ApplicationIntegrityException extends ApplicationException
{
    public function __construct($message = 'Integrity exception', $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
