<?php
namespace bdb\component\exception;

class ApplicationSecurityException extends ApplicationException
{
    public function __construct($message = 'Security exception', $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
