<?php
namespace bdb\component\exception;

class ApplicationServerCommunicationException extends ApplicationException
{
    public function __construct($message = 'Server communication exception', $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
