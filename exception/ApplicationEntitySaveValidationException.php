<?php
namespace bdb\component\exception;

class ApplicationEntitySaveValidationException extends ApplicationException
{
    public function __construct($message = 'Entity save validation exception', $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
