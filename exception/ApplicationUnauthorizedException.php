<?php
namespace bdb\component\exception;

class ApplicationUnauthorizedException extends ApplicationException
{
    public function __construct($message = 'Unauthorized exception', $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
