<?php
namespace bdb\component;

class AttributeWrapper
{
    const TYPE_SUBQUERY = 'TYPE_SUBQUERY';
    const TYPE_EXPRESSION = 'TYPE_EXPRESSION';
    const TYPE_RAW = 'TYPE_RAW';

    var $type;
    var $name;
    var $value;

    function __construct($type, $name, $value)
    {
        $this->type = $type;
        $this->name = $name;
        $this->value = $value;
    }
}