<?php

namespace bdb\component\helper;


class Extra
{
    public static function getImage($url)
    {

        $pathinfo = pathinfo($url);

        if(!file_exists($_SERVER['DOCUMENT_ROOT'].$url) || !isset($pathinfo["extension"]))
        {
            return !isset($pathinfo["extension"]) ? $url."placeholder.png" : $pathinfo["dirname"]."/placeholder.png";
        }
        else
        {
            return $url;
        }
    }
}