<?php

namespace bdb\component\helper;

 
/**
 * This is just an example.
 */
class TicketDump
{
    
    public static function generate($authenticatedSolutionUser, $json, $onError, $onProgress, $onResult){  
      $buff = "";
      $mode = "";
      
      $callback = function ($curlHandle, $data) use (&$buff, &$mode, &$onProgress){        
        $dataLen = strlen($data);
        
        for($i=0; $i < $dataLen; $i++) {
           $c =  $data[$i]; 
           
           if($c == "\n" && $mode != "#2"){
             if($buff == "#1" || $buff == "#2"){
            $mode = $buff;      
             }else if($mode == "#1"){
            $onProgress($buff);
             } 
             
             $buff = "";
           }else{
             $buff .= $c;
           }
        }
     
            return strlen($data);
      };

      $host = "pip.mobi";
      $url = "https://${host}:9971/webservice/TicketDump/generate/${authenticatedSolutionUser}";
      $curlHandle = curl_init();
      $jsonLen = strlen($json);

      curl_setopt($curlHandle, CURLOPT_HTTPHEADER, [
        "Connection: close",
        "X-Connection: close",
        "Content-Type: text/plain",
        "Content-Length: $jsonLen",
        "X-PIP-INTERNAL: df07098dfhklj098089sdpfhklhlkh"
      ]);

      curl_setopt_array($curlHandle, [
          CURLOPT_URL => $url,
          CURLOPT_SSL_VERIFYHOST => false,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_USERAGENT => 'PIP-BACKEND',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_CONNECTTIMEOUT => 120,
          CURLOPT_TIMEOUT        => 120,
          CURLOPT_BUFFERSIZE => 256,
          CURLOPT_POST => true,
          CURLOPT_POSTFIELDS => $json,
          CURLOPT_WRITEFUNCTION => $callback,
          CURLOPT_FAILONERROR => true
      ]);

      $error = false;
      curl_exec($curlHandle) or $error = curl_error($curlHandle);
      $code = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
      curl_close($curlHandle);

      if($code == 200){
        if($error){
          $onError(0, $error);
        }else{
          $onResult($buff);
        } 
      }else{
        $onError($code, "RemoteException");
      }  
    }
}
