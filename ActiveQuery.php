<?php

namespace bdb\component;

use ArrayObject;
use yii\base\InvalidParamException;

class ActiveQueryMetadata
{
    const token = "_";
    public $rootEntity = null;
    public $classes = [];
    public $entities = [];
    public $aliases = [];
    public $fields = [];
    public $ids = [];
    public $kids = [];
    public $tsuuids = [];

    public function getRootEntityRef()
    {
        return $this->aliases[$this->rootEntity];
    }

    public function getFieldRef($field)
    {
        if (isset($fields[$field])) {
            return $fields[$field];
        } else {
            $alias = null;
            $entityField = null;

            if (strpos($field, '_') !== false) {
                $fieldParts = explode("_", $field);
                $alias = $fieldParts[0];
                $entityField = $fieldParts[1];
            } else {
                $entityField = $field;
                $alias = $this->rootEntity;
            }

            $ref = $this->aliases[$alias];
            return $this->registerEntityField($ref->entityAlias, $ref->entityClass, $field, $entityField, $ref->columnsMetadata[$entityField]);
        }
    }

    function registerEntityField($entityAlias, $entityClass, $field, $entityField, $columnMetadata)
    {
        $entity = $entityClass::tableName();
        $labels = $entityClass::modelAttributeLabels();
        $label = isset($labels[$entityField]) ? $labels[$entityField] : 'Unknow';
        $keyType = false;

        if ($columnMetadata->isPrimaryKey) {
            $keyType = 'id';
        } else if ($entityField == 'kid') {
            $keyType = 'kid';
        } else if ($entityField == 'tsuuid') {
            $keyType = 'tsuuid';
        }

        $ref = new ArrayObject([
            'field' => $field, 'label' => $label, 'entity' => $entity, 'entityAlias' => $entityAlias,
            'entityField' => $entityField, 'entityFieldComposite' => ($entityAlias . '.' . $entityField), 'entityClass' => $entityClass,
            'keyType' => $keyType, 'columnMetadata' => $columnMetadata
        ], ArrayObject::STD_PROP_LIST | ArrayObject::ARRAY_AS_PROPS);

        $this->fields[$field] = $ref;

        if ($keyType !== false) {
            if ($keyType == 'id') {
                $this->ids[$field] = $ref;
            } else if ($keyType == 'kid') {
                $this->kids[$field] = $ref;
            } else if ($keyType == 'tsuuid') {
                $this->tsuuids[$field] = $ref;
            }
        }

        return $ref;
    }

    public function registerEntity($entityAlias, $parentEntityAlias, $parentRelationFields,
                                   $entityClass, $entityFields = [], $columnsMetadata = null)
    {
        if (array_key_exists($entityAlias, $this->aliases)) {
            throw new InvalidParamException("entityAlias '$entityAlias' already exists");
        }

        $columnsMetadata = $columnsMetadata != null ? $columnsMetadata : $entityClass::getTableSchema()->columns;
        $entity = $entityClass::tableName();
        $labels = $entityClass::modelAttributeLabels();
        $selectColumns = [];
        $isRootEntity = $entityAlias == $this->rootEntity;

        if (is_array($entityFields)) {
            foreach ($entityFields as $value) {
                $entityField = $value;
                $field = $isRootEntity ? $entityField : "${entityAlias}_${entityField}";

                if (isset($columnsMetadata[$value])) {
                    $selectColumns[$field] = "${entityAlias}.${entityField}";
                    $this->registerEntityField($entityAlias, $entityClass, $field, $entityField, $columnsMetadata[$value]);
                } else {
                    throw new InvalidParamException("Missing column '$value' on entity class '$entityClass' to make field '$field'");
                }
            }
        } else if ($entityFields === true) {
            foreach ($columnsMetadata as $name => $columnMetadata) {
                $entityField = $columnMetadata->name;
                $field = $isRootEntity ? $entityField : "${entityAlias}_${entityField}";
                $selectColumns[$field] = "${entityAlias}.${entityField}";
                $this->registerEntityField($entityAlias, $entityClass, $field, $entityField, $columnMetadata);
            }
        }

        $ref = new ArrayObject([
            'entityAlias' => $entityAlias, 'parentEntityAlias' => $parentEntityAlias, 'parentRelationFields' => $parentRelationFields,
            'entity' => $entity, 'entityClass' => $entityClass, 'labels' => $labels,
            'isRootEntity' => $isRootEntity, 'columnsMetadata' => $columnsMetadata
        ], ArrayObject::STD_PROP_LIST | ArrayObject::ARRAY_AS_PROPS);

        $this->aliases[$entityAlias] = $ref;

        if (!isset($this->classes[$entityClass])) {
            $this->classes[$entityClass] = [];
        }

        if (!isset($this->entities[$entity])) {
            $this->entities[$entity] = [];
        }

        $this->classes[$entityClass][$entityAlias] = $ref;
        $this->entities[$entity][$entityAlias] = $ref;
        return (object)['ref' => $ref, 'selectColumns' => $selectColumns];
    }
}

/**
 * This is the ActiveQuery class for bdb
 * @property ActiveQueryMetadata $metadata
 * @see \yii\db\ActiveQuery
 */
class ActiveQuery extends \yii\db\ActiveQuery
{
    protected $metadata = null;

    /**
     * @return ActiveQueryMetadata | null
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    protected function ensureMetadata()
    {
        if ($this->metadata == null) {
            $this->metadata = new ActiveQueryMetadata();
        }
    }

    /**
     * @inheritdoc
     */
    public function prepare($builder)
    {
        $this->ensureMetadata();

        if ($this->metadata->rootEntity == null) {
            $this->root(true);
        }

        return parent::prepare($builder);
    }

    public function root($fields = false)
    {
        $this->ensureMetadata();
        $entityClass = $this->modelClass;
        $rootEntity = $entityClass::tableName();
        $this->metadata->rootEntity = $rootEntity;
        $this->registerEntity($rootEntity, null, null, $entityClass, $fields);
        return $this->from($rootEntity)->andWhere([$rootEntity . '.deleted' => 0]);
    }

    function registerEntityField($entityAlias, $entityClass, $field, $entityField, $columnMetadata)
    {
        $this->ensureMetadata();
        $this->metadata->registerEntityField($entityAlias, $entityClass, $field, $entityField, $columnMetadata);
        return $this;
    }

    public function registerEntity($entityAlias, $parentEntityAlias, $parentRelationFields,
                                   $entityClass, $entityFields = [], $columnsMetadata = null)
    {
        $this->ensureMetadata();

        $res = $this->metadata->registerEntity(
            $entityAlias, $parentEntityAlias, $parentRelationFields,
            $entityClass, $entityFields, $columnsMetadata
        );

        if (count($res->selectColumns) > 0) {
            $this->addSelect($res->selectColumns);
        }

        return $this;
    }

    public function flatInnerJoinWith($entries, $eagerLoading = true)
    {
        return $this->flatJoinWith($entries, $eagerLoading, 'INNER JOIN');
    }

    public function flatJoinWith($entries, $eagerLoading = true, $joinType = 'LEFT JOIN')
    {
        $rootEntityClass = $this->modelClass;
        $rootEntity = $rootEntityClass::tableName();
        $path = '';
        $parentEntityAlias = $rootEntity;
        $parentEntity = $rootEntity;
        $parentEntityClass = $rootEntityClass;
        $with = [];
        $mainQuery = $this;

        foreach ($entries as $key => $entry) {
            if ($path != '') {
                $path .= '.';
            }

            $entityClass = null;
            $entityFields = false;
            $callable = null;
            $entityAlias = null;

            if (is_array($entry)) {
                foreach ($entry as $k => $v) {
                    if (is_array($v) || is_bool($v)) {
                        $entityFields = $v;
                    } else if (is_callable($v)) {
                        $callable = $v;
                    } else if (is_string($v) && class_exists($v)) {
                        $entityAlias = $k;
                        $entityClass = $v;
                    }
                }
            } else {
                $entityAlias = $key;
                $entityClass = $entry;
            }

            $entity = $entityClass::tableName();
            $lcEntity = lcfirst($entity);
            $path .= $lcEntity;

            if (is_integer($entityAlias)) {
                $entityAlias = $lcEntity;
            }

            if (array_key_exists($entityAlias, $this->metadata->aliases)) {
                $newEntityAlias = null;
                $suffixIdx = 1;

                do {
                    $newEntityAlias = $entityAlias . $suffixIdx;
                    $suffixIdx += 1;
                } while (array_key_exists($newEntityAlias, $this->metadata->aliases));

                $entityAlias = $newEntityAlias;
            }

            $with[$path] = function ($query) use ($mainQuery, $entityAlias, $entity, $callable, $path, $entityClass, $entityFields) {
                if ($callable == null) {
                    $query->from([$entityAlias => $entity])->andWhere([$entityAlias . '.deleted' => 0]);
                } else {
                    call_user_func($callable, (object)[
                        'mainQuery' => $mainQuery, 'query' => $query, 'entityAlias' => $entityAlias, 'entity' => $entity,
                        'path' => $path, 'entityClass' => $entityClass, 'entityFields' => $entityFields
                    ]);
                }
            };

            $parentRelationFields = ['id' => lcfirst($entity) . 'Id'];
            $this->registerEntity($entityAlias, $parentEntityAlias, $parentRelationFields, $entityClass, $entityFields);
            $parentEntityAlias = $entityAlias;
            $parentEntity = $entity;
            $parentEntityClass = $entityClass;
        }

        if ($path == '') {
            return $this;
        } else {
            return $this->joinWith($with, $eagerLoading, $joinType);
        }
    }
}
