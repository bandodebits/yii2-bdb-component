<?php
namespace bdb\component\collection;

use bdb\component\lang\LightComponent;

class Tuple
{
    use LightComponent;

    public function __construct()
    {
        $this->attributes = func_get_args();
    }
}