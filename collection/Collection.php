<?php

namespace bdb\component\collection;


use bdb\component\lang\Unit;
use yii\base\InvalidParamException;

class Collection
{
    public static function map()
    {
        $result = [];
        $array = func_get_arg(0);

        if (func_num_args() == 2) {
            $arg1 = func_get_arg(1);

            if (is_callable($arg1)) {
                foreach ($array as $k => $v) {
                    $bodyResult = call_user_func($arg1, $k, $v);

                    if ($bodyResult != Unit::$VALUE) {
                        if ($bodyResult instanceof Pair) {
                            $result[$bodyResult->first] = $bodyResult->second;
                        } else {
                            $result[] = $bodyResult;
                        }
                    }
                }
            } else {
                foreach ($array as $k => $v) {
                    $result[] = $v[$arg1];
                }
            }
        } else if (func_num_args() == 3) {
            $keyField = func_get_arg(1);
            $valueField = func_get_arg(2);

            foreach ($array as $k => $v) {
                $result[$v[$keyField]] = $v[$valueField];
            }
        } else {
            throw new InvalidParamException("Invalid params passed");
        }

        return $result;
    }
}