<?php
namespace bdb\component\collection;


class Pair extends Tuple
{
    function __construct($first, $second)
    {
        parent::__construct($first, $second);
    }

    public function getFirst()
    {
        return $this->attributes[0];
    }

    public function setFirst($value)
    {
        return $this->attributes[0] = $value;
    }

    public function getSecond()
    {
        return $this->attributes[1];
    }

    public function setSecond($value)
    {
        return $this->attributes[1] = $value;
    }
}