<?php
namespace bdb\component;

use bdb\component\exception\ApplicationException;
use bdb\component\exception\ApplicationIntegrityException;
use bdb\component\exception\ApplicationSecurityException;
use bdb\component\exception\ApplicationValidationException;
use yii;

trait SearchModelTrait
{
    public function buildModelStateUrlParams()
    {
        $name = $this->formName();
        $result = '';
        $count = 0;

        foreach ($this->attributes as $field => $value) {
            if (!empty($value)) {

                if ($field == 'id') {
                    throw new ApplicationSecurityException('"id" is an internal field');
                }

                $result .= '&';
                $count += 1;
                $result .= urlencode($name . "[$field]") . '=' . urlencode($value);
            }
        }

        return '-c=' . $count . $result;
    }

    public function getFromKeyValueField($query, $keyField, $valueField)
    {
        $ref = $query->metadata->fields[$keyField];
        $entityClass = $ref->entityClass;
        $entityKeyField = null;
        $entityValueField = null;

        if (strpos($keyField, '_') !== false) {
            $fieldParts = explode("_", $keyField);
            $entityKeyField = $fieldParts[1];
        } else {
            $entityKeyField = $valueField;
        }

        if (strpos($valueField, '_') !== false) {
            $fieldParts = explode("_", $valueField);
            $entityValueField = $fieldParts[1];
        } else {
            $entityValueField = $valueField;
        }

        $specs = $this->defaultSecuritySpecs();
        $authAccountId = \Yii::$app->user->identity->id;
        $filters = ['and'];
        $finderQuery = $entityClass::find()->root([$entityValueField]);
        $spec = isset($specs[$entityClass]) ? $specs[$entityClass] : SearchSecuritySpecs::DEF;
        $this->addSecurityFilters($query, $authAccountId, null, $spec, $filters);
        return $finderQuery->where($filters)->andWhere(['=', $entityKeyField, $this->$keyField])->createCommand()->queryScalar();
    }

    public function configureSearchQueryWhereClauseSecurity($query, $securitySpecs = null)
    {
        $specs = $securitySpecs == null ? $this->defaultSecuritySpecs() : $securitySpecs;
        $authAccountId = \Yii::$app->user->isGuest ? null : \Yii::$app->user->identity->id;
        $filters = ['and'];

        foreach ($query->metadata->aliases as $alias => $ref) {
            $spec = isset($specs[$ref->entityClass]) ? $specs[$ref->entityClass] : SearchSecuritySpecs::DEF;
            $this->addSecurityFilters($query, $authAccountId, $alias, $spec, $filters);
        }

        $query->andWhere($filters);
    }

    protected function addSecurityFilters($query, $authAccountId, $alias, $spec, &$filters)
    {
        $prefix = $alias != null ? $alias . '.' : '';

        if (is_callable($spec)) {
            $res = call_user_func($spec, $query, $authAccountId, $alias, $prefix);

            if (is_array($res)) {
                $filters[] = $res;
            }
        } else {
            $secs = null;

            if (is_string($spec)) {
                $secs = [$spec];
            } else if (is_array($spec)) {
                $secs = $spec;
            } else {
                throw new ApplicationException("addSecurityFilters unknow spec type: ${$spec}");
            }

            foreach ($secs as $sec) {
                switch ($sec) {
                    case SearchSecuritySpecs::OWNER :
                        $filters[] = ['=', $prefix . 'authAccountIdOwner', $authAccountId];
                        break;
                    case SearchSecuritySpecs::OPT_OWNER :
                        $filters[] = ['OR', [$prefix . 'authAccountIdOwner' => $authAccountId], [$prefix . 'authAccountIdOwner' => null]];
                        break;
                    case SearchSecuritySpecs::CREATOR :
                        $filters[] = ['=', $prefix . 'authAccountIdCreate', $authAccountId];
                        break;
                    case SearchSecuritySpecs::OPT_CREATOR :
                        $filters[] = ['OR', [$prefix . 'authAccountIdCreate' => $authAccountId], [$prefix . 'authAccountIdCreate' => null]];
                        break;
                    case SearchSecuritySpecs::UPDATER :
                        $filters[] = ['=', $prefix . 'authAccountIdUpdate', $authAccountId];
                        break;
                    case SearchSecuritySpecs::OPT_UPDATER :
                        $filters[] = ['OR', [$prefix . 'authAccountIdUpdate' => $authAccountId], [$prefix . 'authAccountIdUpdate' => null]];
                        break;
                }
            }
        }
    }

    public function defaultModelClassConverter()
    {
        return function ($row, $fieldsMetadata) {
            return new DynamicModel($row);
        };
    }

    public function defaultSecuritySpecs()
    {
        return [];
    }

    public function keyField()
    {
        return 'tsuuid';
    }

    /**
     * @return ActiveQuery
     */
    abstract public function defaultBaseQuery();

    /**
     * @param $query
     * @param null|array $securitySpecs
     * @param array $excludes
     * @throws ApplicationSecurityException
     */
    public function configureSearchQueryWhereClause($query, $securitySpecs = null, $excludes = null)
    {
        $this->configureSearchQueryWhereClauseSecurity($query, $securitySpecs);
        $filters = ['and'];

        foreach ($this->activeAttributes() as $field) {
            $value = $this->$field;

            if ($value != null && ($excludes == null || !in_array($field, $excludes))) {
                $ref = $query->metadata->getFieldRef($field);

                if ($ref->keyType == 'id') {
                    throw new ApplicationSecurityException("Validation: '${field}' is an internal field");
                } else if ($ref->keyType == 'tsuuid') {
                    $filters[] = ['=', $ref->entityFieldComposite, $value];
                } else {
                    $type = $ref->columnMetadata->type;

                    switch ($type) {
                        case 'string':
                            $filters[] = ['like', $ref->entityFieldComposite, $value];
                            break;
                        case 'datetime':
                            $dateRange = explode('-', $value);

                            $filters[] = ['between',
                                $ref->entityFieldComposite, Yii::$app->formatter->asDate($dateRange[0], 'Y-MM-dd'),
                                Yii::$app->formatter->asDate($dateRange[1], 'Y-MM-dd')
                            ];

                            break;
                    }
                }
            }
        }

        $query->andWhere($filters);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return FlatDataProvider
     * @throws ApplicationValidationException
     */
    public function search($params)
    {
        $loadedResult = $this->load($params);
        $dataProvider = $this->searchFacade();

        if (!$loadedResult) {
            return $dataProvider;
        } else if (!$this->validate()) {
            throw new ApplicationValidationException("Validation fail");
        } else {
            return $dataProvider;
        }
    }

    public function searchFacade($query = null, $attributes = null, $securitySpecs = null, $modelClassConverter = null)
    {
        if ($query == null) {
            $query = $this->defaultBaseQuery();
        }

        if ($attributes == null) {
            $attributes = array_unique(array_merge($this->activeAttributes(), $this->internalAttributes()));
        }

        $this->configureSearchQueryWhereClause($query, $securitySpecs);
        $rootEntity = $query->metadata->rootEntity;
        $selectColumns = [];

        foreach ($attributes as $attribute) {
            $isAttributeWrapper = !is_string($attribute) && get_class($attribute) == AttributeWrapper::class;
            $field = $isAttributeWrapper ? $attribute->name : $attribute;
            $alias = null;
            $entityField = null;

            if (strpos($field, '_') !== false) {
                $fieldParts = explode("_", $field);
                $alias = $fieldParts[0];
                $entityField = $fieldParts[1];
            } else {
                $alias = $rootEntity;
                $entityField = $field;
            }

            if ($isAttributeWrapper) {
                if ($attribute->type == AttributeWrapper::TYPE_SUBQUERY ||
                    $attribute->type == AttributeWrapper::TYPE_EXPRESSION ||
                    $attribute->type == AttributeWrapper::TYPE_RAW
                ) {
                    $selectColumns[$field] = $attribute->value;
                } else {
                    throw new ApplicationIntegrityException("AttributeWrapper type unknow: " . $attribute->type);
                }
            } else {
                $selectColumns[$field] = "$alias.$entityField";
            }

            $ref = $query->metadata->aliases[$alias];
            $query->registerEntityField($ref->entityAlias, $ref->entityClass, $field, $entityField, $ref->columnsMetadata[$entityField]);
        }

        $query->select($selectColumns);

        $dataProviter = new FlatDataProvider([
            'query' => $query,
            'modelClassConverter' => $modelClassConverter != null ? $modelClassConverter : $this->defaultModelClassConverter(),
            'key' => $this->keyField()
        ]);

        $dataProviter->configureSort($attributes);
        return $dataProviter;
    }
}
