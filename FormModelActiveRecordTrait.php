<?php
namespace bdb\component;

use Yii;

trait FormModelActiveRecordTrait
{
    use FormModelTrait;

    /**
     * Transactional method to save from post.
     *
     * @param function ($model)|null $beforeSave
     * @param function ($model)|null $afterSave
     * @param function ($model)|null $afterCommit
     * @return array, and sets Yii::$app->response->format = Response::FORMAT_JSON;
     * @throws \yii\db\Exception
     */
    public function saveFromPost($beforeSave = null, $afterSave = null, $afterCommit = null, $bypassEmpty = false)
    {
        $this->loadFromPost($bypassEmpty);

        if ($this->validate()) {
            $transaction = Yii::$app->getDb()->beginTransaction();

            try {
                if ($beforeSave != null) {
                    call_user_func_array($beforeSave, [&$this]);
                }

                if (!$this->hasErrorsExtra()) {
                    if ($this->save()) {
                        $this->addExtraData('id', $this->tsuuid);

                        if ($afterSave != null) {
                            call_user_func_array($afterSave, [&$this]);
                        }
                    } else {
                        $this->addExtraWarning(Yii::t('app', "Not saved"));
                    }
                }
            } catch (\Exception $e) {
                $this->addExtraException($e);
            }

            if ($this->hasErrorsExtra()) {
                $transaction->rollBack();
            } else {
                $transaction->commit();

                if ($afterCommit != null) {
                    try {
                        call_user_func_array($afterCommit, [&$this]);
                    } catch (\Exception $e) {
                        $this->addExtraException($e);
                    }
                }

                if (!$this->hasErrorsExtra()) {
                    $this->addExtraMessage(Yii::t('app', "Done"));
                }
            }
        }

        return $this;
    }
}
