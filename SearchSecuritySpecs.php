<?php
namespace bdb\component;

class SearchSecuritySpecs
{
    const BYPASS = 'BYPASS';

    const OPEN = 'OPEN';

    const OWNER = 'OWNER';

    const OPT_OWNER = 'OPT_OWNER';

    const CREATOR = 'CREATOR';

    const OPT_CREATOR = 'OPT_CREATOR';

    const UPDATER = 'UPDATER';

    const OPT_UPDATER = 'OPT_UPDATER';

    const DEF = self::OWNER;
}
