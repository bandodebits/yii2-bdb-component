<?php
namespace bdb\component;

use bdb\component\exception\ApplicationException;
use bdb\component\lang\Unit;
use yii\widgets\Pjax;

class AjaxHelper
{
    public static function renderPjaxContainer($renderer, $viewBasePath, $id, $data, $params = [])
    {
        $container = str_replace('#', '', \Yii::$app->request->headers['X-PJAX-Container']);
        $pjaxFragmentId = "$id-fragment";
        $pjaxPartialId = "$id-partial";
        $compositeData = array_merge($data, ['pjaxId' => $id, 'pjaxFragmentId' => $pjaxFragmentId, 'pjaxPartialId' => $pjaxPartialId]);
        $viewToRender = empty($viewBasePath) ? $id : "$viewBasePath/$id";

        if (!\Yii::$app->request->isPjax || $container == $id) {
            $params['id'] = $id;
            $pjaxWidget = Pjax::begin($params);
            echo $renderer->render($viewToRender, $compositeData);
            Pjax::end();
            return $pjaxWidget;
        } else if ($container == $pjaxFragmentId || $container == "$pjaxFragmentId-pjax") {
            echo $renderer->renderAjax($viewToRender, $compositeData);
            return Unit::$VALUE;
        } else if ($container == $pjaxPartialId || $container == "$pjaxPartialId-pjax") {
            echo $renderer->render($viewToRender, $compositeData);
            return Unit::$VALUE;
        } else {
            throw new ApplicationException("Unknow pjax container: $container");
        }
    }

    public static function renderAjaxContainer($renderer, $viewBasePath, $id, $data)
    {
        echo "<div id='$id-container'><div id='$id'>";
        echo $renderer->renderAjax(empty($viewBasePath) ? $id : "$viewBasePath/$id", array_merge($data, ['ajaxId' => $id]));
        echo "</div></div>";
        return Unit::$VALUE;
    }
}

