<?php
namespace bdb\component;

class Notification {

	static function setOptions ($options = [])
	{
		$result = 'var opts = {
		"closeButton": '.(isset($options['closeButton']) ? $options['closeButton'] : 'true').',
		"debug": '.(isset($options['debug']) ? $options['debug'] : 'false').',
		"positionClass": "'.(isset($options['positionClass']) ? $options['positionClass'] : 'toast-bottom-right').'",
		"onclick": '.(isset($options['onclick']) ? $options['onclick'] : 'null').',
		"showDuration": "'.(isset($options['showDuration']) ? $options['showDuration'] : '300').'",
		"hideDuration": "'.(isset($options['hideDuration']) ? $options['hideDuration'] : '1000').'",
		"timeOut": "'.(isset($options['timeOut']) ? $options['timeOut'] : '5000').'",
		"extendedTimeOut": "'.(isset($options['extendedTimeOut']) ? $options['extendedTimeOut'] : '1000').'",
		"showEasing": "'.(isset($options['showEasing']) ? $options['showEasing'] : 'swing').'",
		"hideEasing": "'.(isset($options['hideEasing']) ? $options['hideEasing'] : 'linear').'",
		"showMethod": "'.(isset($options['showMethod']) ? $options['showMethod'] : 'fadeIn').'",
		"hideMethod": "'.(isset($options['hideMethod']) ? $options['hideMethod'] : 'fadeOut').'"
		};';

		return $result;
	}

	static function info($content)
	{
		print '<script>';
		print "toastr.info('{$content}');";
		print '</script>';
	}	

	static function success($options = [])
	{
		print '<script>';
		print self::setOptions($options);
		print 'toastr.success("'.(isset($options['content']) ? $options['content'] : '').'", "'.(isset($options['title']) ? $options['title'] : '').'", opts);';
		print '</script>';
	}	

	static function error($options = [])
	{
		print '<script>';
		print self::setOptions($options);
		print 'toastr.error("'.(isset($options['content']) ? $options['content'] : '').'", "'.(isset($options['title']) ? $options['title'] : '').'", opts);';
		print '</script>';
	}	

	static function warning($options = [])
	{
		print '<script>';
		print self::setOptions($options);
		print 'toastr.warning("'.(isset($options['content']) ? $options['content'] : '').'", "'.(isset($options['title']) ? $options['title'] : 'null').'", opts);';
		print '</script>';
	}
}

/*
EXEMPLO ----------------------------------------------

\bdb\component\Notification::info('Olá Mundo');
\bdb\component\Notification::success(['title' => 'titulo', 'content' => 'Olá mundo', 'positionClass' => 'toast-top-full-width']);
*/