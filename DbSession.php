<?php

namespace bdb\component;

use Yii;
use yii\web\Session;
use yii\db\Query;
use yii\di\Instance;
use yii\db\Connection;
/**
 * This is just an example.
 */
class DbSession extends \yii\web\DbSession 
{


     
 
 	/**
     * Updates the current session ID with a newly generated one .
     * Please refer to <http://php.net/session_regenerate_id> for more details.
     * @param boolean $deleteOldSession Whether to delete the old associated session file or not.
     */
    public function regenerateID($deleteOldSession = false)
    {
        $oldID = session_id();

        // if no session is started, there is nothing to regenerate
        if (empty($oldID)) {
            return;
        }

        Session::regenerateID(false);
        $newID = session_id();

        $query = new Query;
        $row = $query->from($this->sessionTable)
            ->where(['id' => $oldID])
            ->createCommand($this->db)
            ->queryOne();
        if ($row !== false) {
            if ($deleteOldSession) {

            	if(!is_null(Yii::$app->user->identity))
            	{
            		$this->db->createCommand()
                	//->delete($this->sessionTable)
                    ->update($this->sessionTable, [
                    	'id' => $newID, 
                    	'AuthAccount' => @Yii::$app->user->identity->id,
                        'appId' => Yii::$app->id,
                    	'ipAddress' => Yii::$app->request->userIp,
                    	'userAgent' => Yii::$app->request->userAgent,
                    	'serverPort' => Yii::$app->request->serverPort,
                    	'serverName' => Yii::$app->request->serverName,
                    	'method' => Yii::$app->request->method
                    ], ['id' => $oldID])
                    ->execute();
            	} else 
            	{
            		 $this->db->createCommand()
                	->delete($this->sessionTable)
                    ->execute();
            	}

               
            } else {
            	//DUVIDA
                $row['id'] = $newID;
                $row['AuthAccount'] = Yii::$app->user->identity->id;
                $row['ipAddress'] = Yii::$app->request->userIp;
                $row['userAgent'] = Yii::$app->request->userAgent;
                $row['serverPort'] = Yii::$app->request->serverPort;
                $row['serverName'] = Yii::$app->request->serverName;
                $row['method'] = Yii::$app->request->method;
                $this->db->createCommand()
                    ->insert($this->sessionTable, $row)
                    ->execute();
            }
        } else {
        	//DUVIDA
            // shouldn't reach here normally
            $this->db->createCommand()
                ->insert($this->sessionTable, [
                    'id' => $newID,
                    'AuthAccount' => NULL, 
                	'ipAddress' => Yii::$app->request->userIp,
                	'userAgent' => Yii::$app->request->userAgent,
                	'serverPort' => Yii::$app->request->serverPort,
                	'serverName' => Yii::$app->request->serverName,
                	'method' => Yii::$app->request->method,
                    'expire' => time() + $this->getTimeout(),
                ])->execute();
        }
    }



    /**
     * Session write handler.
     * Do not call this method directly.
     * @param string $id session ID
     * @param string $data session data
     * @return boolean whether session write is successful
     */
    public function writeSession($id, $data)
    {
        // exception must be caught in session write handler
        // http://us.php.net/manual/en/function.session-set-save-handler.php
        try {
            $expire = time() + $this->getTimeout();
            $query = new Query;
            $exists = $query->select(['id'])
                ->from($this->sessionTable)
                ->where(['id' => $id])
                ->createCommand($this->db)
                ->queryScalar();
            if ($exists === false) {
                $this->db->createCommand()
                    ->insert($this->sessionTable, [
                        'id' => $id,
                        'data' => $data,
                        'expire' => $expire,
	                	'ipAddress' => Yii::$app->request->userIp,
	                	'userAgent' => Yii::$app->request->userAgent,
	                	'serverPort' => Yii::$app->request->serverPort,
	                	'serverName' => Yii::$app->request->serverName,
	                	'method' => Yii::$app->request->method,
	                    'expire' => time() + $this->getTimeout(),
                    ])->execute();
            } else {
                $this->db->createCommand()
                    ->update($this->sessionTable, [
                    	'data' => $data, 
                    	'expire' => $expire,
                    	'ipAddress' => Yii::$app->request->userIp,
                    	'userAgent' => Yii::$app->request->userAgent,
                    	'serverPort' => Yii::$app->request->serverPort,
                    	'serverName' => Yii::$app->request->serverName,
                    	'method' => Yii::$app->request->method
                    ], ['id' => $id])
                    ->execute();
            }
        } catch (\Exception $e) {
            $exception = ErrorHandler::convertExceptionToString($e);
            // its too late to use Yii logging here
            error_log($exception);
            echo $exception;

            return false;
        }

        return true;
    }
	 
}
