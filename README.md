component
=========
component

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist bandodebits/yii2-bdb-component "*"
```

or add

```
"bandodebits/yii2-bdb-component": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \bdb\component\AutoloadExample::widget(); ?>```