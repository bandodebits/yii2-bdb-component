<?php

namespace bdb\component;

use bdb\component\exception\ApplicationEntitySaveValidationException;
use bdb\component\exception\ApplicationIntegrityException;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Json;

class ActiveRecord extends \yii\db\ActiveRecord
{
    use ModelTrait;

    public function optimisticLock()
    {
        return 'rowVersion';
    }


    public function beforeSave($insert)
    {
        $internalServiceIntegration = new InternalServiceIntegration();

        if ($insert) {
            $tsuuid = $internalServiceIntegration->tsuuid();

            if ($tsuuid !== false) {
                $tsid = substr($tsuuid, 0, 22);
                $this->rowVersion = 0;

                if (in_array('authAccountIdCreate', $this->attributes()) && !\yii::$app->user->isGuest) $this->authAccountIdCreate = \Yii::$app->user->identity->id;
                if (in_array('authAccountIdOwner', $this->attributes()) && !\yii::$app->user->isGuest) $this->authAccountIdOwner = \Yii::$app->user->identity->id;
                if (in_array('tsuuid', $this->attributes())) $this->tsuuid = $tsuuid;
                if (in_array('tsidCreate', $this->attributes())) $this->tsidCreate = $tsid;
                if (in_array('tsCreate', $this->attributes())) $this->tsCreate = date("Y-m-d H:i:s");
                if (in_array('dtCreate', $this->attributes())) $this->dtCreate = date("Y-m-d");
            } else {
                throw new InvalidConfigException('tsuuid failed');
            }
        } else {
            $tsid = $internalServiceIntegration->tsid();

            if ($tsid !== false) {
                if (in_array('tsidUpdate', $this->attributes())) $this->tsidUpdate = $tsid;
                if (in_array('authAccountIdUpdate', $this->attributes()) && !\yii::$app->user->isGuest) $this->authAccountIdUpdate = \Yii::$app->user->identity->id;
                if (in_array('tsUpdate', $this->attributes())) $this->tsUpdate = date("Y-m-d H:i:s");
                if (in_array('dtUpdate', $this->attributes())) $this->dtUpdate = date("Y-m-d");
            } else {
                throw new InvalidConfigException('tsid failed');
            }
        }

        return parent::beforeSave($insert);
    }

    public static function findTsuuId($tsuuid, $lock = false)
    {
        if ($lock) {
            $id = Yii::$app->getDb()->createCommand('
                SELECT id FROM ' . static::tableName() . ' WHERE deleted = 0 AND tsuuid = :tsuuid
            ', [
                ':tsuuid' => $tsuuid
            ])->queryScalar();

            if ($id != null) {
                if (static::acquireLock($id)) {
                    return static::findOne(['id' => $id]);
                } else {
                    throw new \InvalidArgumentException('Can not acquire lock');
                }
            } else {
                return null;
            }
        } else {
            return static::findOne(['tsuuid' => $tsuuid]);
        }
    }

    public static function findById($id, $lock = false)
    {
        if ($lock) {
            $id = Yii::$app->getDb()->createCommand('
                SELECT id FROM ' . static::tableName() . ' WHERE deleted = 0 AND id = :id
            ', [
                ':id' => $id
            ])->queryScalar();

            if ($id != null) {
                if (static::acquireLock($id)) {
                    return static::findOne(['id' => $id]);
                } else {
                    throw new \InvalidArgumentException('Can not acquire lock');
                }
            } else {
                return null;
            }
        } else {
            return static::findOne(['id' => $id]);
        }
    }

    public static function findOneOwnedByTsuuid($tsuuid, $lock = false)
    {
        $authAccountId = \Yii::$app->user->identity->id;

        if ($lock) {
            $id = Yii::$app->getDb()->createCommand('
                SELECT id FROM ' . static::tableName() . ' WHERE deleted = 0 AND tsuuid = :tsuuid AND authAccountIdOwner = :authAccountIdOwner
            ', [
                ':tsuuid' => $tsuuid, ':authAccountIdOwner' => $authAccountId
            ])->queryScalar();

            if ($id != null) {
                if (static::acquireLock($id)) {
                    return static::findOne(['id' => $id]);
                } else {
                    throw new \InvalidArgumentException('Can not acquire lock');
                }
            } else {
                return null;
            }
        } else {
            return static::findOne(['tsuuid' => $tsuuid, 'authAccountIdOwner' => $authAccountId]);
        }
    }

    /**
     * @inheritdoc
     * @return ActiveQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }

    public static function findOwned()
    {
        return (new ActiveQuery(get_called_class()))->where(['authAccountIdOwner' => \Yii::$app->user->identity->id]);
    }

    public static function acquireLock($id)
    {
        return Yii::$app->getDb()->createCommand('SELECT id FROM ' . static::tableName() . 'Lock WHERE id = :id FOR UPDATE', [
            ':id' => $id,
        ])->queryScalar();
    }

    public function lock()
    {
        $this::acquireLock($this->id);
        $this->refresh();
    }

    public function delete()
    {
        $result = $this->getDb()->createCommand('UPDATE ' . $this::tableName() . ' SET deleted = 1 WHERE id = :id', [
            ':id' => $this->id,
        ])->execute();

        if ($result) {
            $this->deleted = 1;
        }

        return $result;
    }

    public function hardDelete()
    {
        return parent::delete();
    }

    public function saveWithValidation()
    {
        if ($this->save()) {
            return true;
        } else {
            throw new ApplicationEntitySaveValidationException(
                'Validation failed when saving: ' . static::tableName() .
                Json::encode($this->getErrors())
            );
        }
    }
}
