<?php

namespace bdb\component;

use yii\base\InvalidCallException;
use yii\base\UnknownMethodException;
use yii\base\UnknownPropertyException;

class DynamicFormModel extends DynamicModel
{
    use FormModelTrait;
}