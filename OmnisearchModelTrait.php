<?php
namespace bdb\component;

use bdb\component\exception\ApplicationValidationException;
use yii\base\InvalidParamException;


trait OmnisearchModelTrait
{
    public function omnisearch($params)
    {
        $this->load($params);

        if (!$this->validate()) {
            throw new ApplicationValidationException();
        } else {
            if (!isset($params['-s'])) {
                $params['-s'] = '';
            }

            if (!isset($params['-p'])) {
                $params['-p'] = 1;
            }

            $target = Util::sqlSafe($params['-t']);//SAFETY: check
            $field = Util::sqlSafe($params['-f']);//SAFETY: check
            $page = (int)Util::sqlSafe($params['-p']);//SAFETY: check
            $limit = (int)Util::sqlSafe($params['-l']);//SAFETY: check
            $search = trim(Util::sqlSafe($params['-s']));//SAFETY: check

            if ($limit > 100) {
                throw new InvalidParamException("Unauthorized limit $limit, max=100");
            }

            if ($this->validateOmnisearch($field, $page, $limit, $search)) {
                switch ($target) {
                    case 'typeahead' :
                        return $this->typeaheadSearch($field, $page, $limit, $search);
                        break;
                    case 'dropdown' :
                        return $this->dropdownSearch($field, $page, $limit, $search);
                        break;
                    default :
                        throw new InvalidParamException("Unknow omniSearch target: $target");
                }
            } else {
                throw new InvalidParamException("Validation error: omnisearch");
            }
        }
    }

    public function validateOmnisearch($field, $page, $limit, $search)
    {
        return in_array($field, $this->activeAttributes());
    }

    public abstract function configureSearchQueryWhereClause($query, $securitySpecs = null, $excludes = null);

    /**
     * Override this to implement typeaheadSearch specialization,
     *  Ex: call doTypeaheadActiveRecordlSearchOwned instead doTypeaheadActiveRecordSearch.
     *
     * @param string $field , subject
     * @param array , $omniSearchRule
     * @param int $limit , int limit
     * @param string $search , predicate
     * @return string , json content
     */
    public function typeaheadSearch($field, $page, $limit, $search)
    {
        return $this->doTypeaheadModelSearchDirect($field, $page, $limit, $search);
    }

    /**
     * Override this to implement dropdownSearch specialization.
     *
     * @param string $field , subject
     * @param array , $omniSearchRule
     * @param int $limit , int limit
     * @param string $search , predicate
     * @return string , json content
     */
    public
    function dropdownSearch($field, $page, $limit, $search)
    {
        return $this->doDropdownModelSearchDirect($field, $page, $limit, $search);
    }

    /**
     * @param $field
     * @param $page
     * @param int $limit
     * @param string $search
     * @param ActiveQuery $query
     * @param function ($params){} $queryAugument
     * @return string
     */
    public
    function doTypeaheadModelSearchDirect($field, $page, $limit, $search, $query = null, $queryAugument = null)
    {
        $modelEntity = null;
        $modelClass = null;

        if ($query == null) {
            $modelClass = get_class($this);
        } else {
            $modelClass = $query->modelClass;
        }

        $modelEntity = $modelClass::tableName();
        $fieldParts = explode("_", $field);
        $alias = null;
        $directField = null;

        if (count($fieldParts) == 2) {
            $directField = $fieldParts[1];
            $alias = $fieldParts[0];
        } else {
            $directField = $field;
            $alias = $modelEntity;
        }

        $entityFieldComposite = "${alias}.${directField}";

        if ($query == null) {
            $query = $modelClass::find()->root([$directField])->distinct(true);
        } else {
            $query->select([$entityFieldComposite])->distinct(true);
        }

        $this->configureSearchQueryWhereClause($query, null, [$field]);

        if ($queryAugument != null) {
            call_user_func($queryAugument, $query);
        }

        $filters = ['and'];
        $filters[] = ['like', $entityFieldComposite, $search . '%', false];

        $query2 = clone $query;
        $data = $query2->andWhere($filters)->orderBy($entityFieldComposite)->limit($limit)->createCommand()->queryAll();
        $out = [];

        foreach ($data as $d) {
            $out[] = ['value' => trim($d[$directField])];
        }

        if (count($out) < $limit) {
            $filters = ['and'];
            $filters[] = ['like', $entityFieldComposite, $search];
            $filters[] = ['not like', $entityFieldComposite, $search . '%', false];

            $query3 = clone $query;
            $data = $query3->andWhere($filters)->
            orderBy($entityFieldComposite)->limit($limit - count($out))->
            createCommand()->queryAll();

            foreach ($data as $d) {
                $out[] = ['value' => trim($d[$directField])];
            }
        }

        return $out;
    }

    /**
     * @param $field
     * @param $page
     * @param int $limit
     * @param string $search
     * @param ActiveQuery $query
     * @param function ($params){} $queryAugument
     * @return string
     */
    public
    function doDropdownModelSearchDirect($field, $page, $limit, $search, $query = null, $queryAugument = null)
    {
        $modelEntity = null;
        $modelClass = null;

        if ($query == null) {
            $modelClass = get_class($this);
        } else {
            $modelClass = $query->modelClass;
        }

        $modelEntity = $modelClass::tableName();
        $fieldParts = explode("_", $field);
        $alias = null;
        $directField = null;
        $direcTsuuid = 'tsuuid';
        $excludeTsuuid = null;

        if (count($fieldParts) == 2) {
            $directField = $fieldParts[1];
            $alias = $fieldParts[0];
            $excludeTsuuid = $alias . '_' . 'tsuuid';
        } else {
            $directField = $field;
            $alias = $modelEntity;
            $excludeTsuuid = 'tsuuid';
        }

        $entityFieldComposite = "${alias}.${directField}";
        $entityTsuuidComposite = "${alias}.tsuuid";

        if ($query == null) {
            $query = $modelClass::find()->root(['tsuuid', $directField])->distinct(true);
        } else {
            $query->select([$entityTsuuidComposite, $entityFieldComposite])->distinct(true);
        }

        $this->configureSearchQueryWhereClause($query, null, [$field, $excludeTsuuid]);

        if ($queryAugument != null) {
            call_user_func($queryAugument, $query);
        }

        $filters = ['and'];
        $filters[] = ['like', $entityFieldComposite, $search];
        $query2 = clone $query;
        $query2->andWhere($filters);
        $count = $query2->count('*');
        $data = $query2->orderBy($entityFieldComposite)->offset($limit * ($page - 1))->limit($limit)->createCommand()->queryAll();
        $out = [];

        foreach ($data as $d) {
            $out[] = ['id' => trim($d[$direcTsuuid]), 'text' => trim($d[$directField])];
        }

        return ['totalCount' => $count, 'incompleteResults' => false, 'results' => $out];
    }
}