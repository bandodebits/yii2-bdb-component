<?php
namespace bdb\component\lang;

use yii\base\UnknownPropertyException;


trait LightComponent
{
    public $attributes = null;

    public function __get($name)
    {
        $getter = 'get' . ucfirst($name);

        if (method_exists($this, $getter)) {
            return $this->$getter();
        } else if (isset($this->attributes[$name])) {
            return $this->attributes[$name];
        } else if (strlen($name) > 1 && String::startsWith($name, '_')) {
            $position = substr($name, 1, strlen($name) - 1);

            if (is_integer($position)) {
                return $this->attributes[((int)$position) - 1];
            } else {
                throw new UnknownPropertyException('Getting unknown property: ' . get_class($this) . '::' . $name);
            }
        } else {
            throw new UnknownPropertyException('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setter = 'set' . ucfirst($name);

        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } else if (isset($this->attributes[$name])) {
            $this->attributes[$name] = $value;
        } else if (strlen($name) > 1 && String::startsWith($name, '_')) {
            $position = substr($name, 1, strlen($name) - 1);

            if (is_integer($position)) {
                $this->attributes[((int)$position) - 1] = $value;
            } else {
                throw new UnknownPropertyException('Setting unknown property: ' . get_class($this) . '::' . $name);
            }
        } else {
            throw new UnknownPropertyException('Setting unknown property: ' . get_class($this) . '::' . $name);
        }
    }
}
