<?php
namespace bdb\component\lang;

class String
{
    public static function startsWith($_this, $fragment)
    {
        return strncmp($_this, $fragment, strlen($fragment)) === 0;
    }

    public static function endsWith($_this, $fragment)
    {
        return $fragment === '' || substr_compare($_this, $fragment, -strlen($fragment)) === 0;
    }
}