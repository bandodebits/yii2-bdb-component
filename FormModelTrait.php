<?php
namespace bdb\component;

use kartik\helpers\Html;
use Yii;
use yii\base\Model;
use yii\web\Response;

trait FormModelTrait
{
    public $extraData = [];
    public $extraMessages = [];
    public $extraWarnings = [];
    public $extraWarningsPassthrough = [];
    public $extraErrors = [];
    public $extraErrorsPassthrough = [];
    public $extraExceptions = [];
    public $extraExceptionsPassthrough = [];

    /**
     * Load non empty active attributes
     * @param bool $bypassEmpty
     * @return Model instance
     */
    public function loadFromPost($bypassEmpty = false)
    {
        $formName = $this->formName();
        $post = \Yii::$app->request->post();
        $formFieldsDataFiltered = [];

        if (isset($post[$formName])) {
            $formFieldsData = $post[$formName];

            foreach ($this->activeAttributes() as $field) {
                if (isset($formFieldsData[$field])) {
                    $v = $formFieldsData[$field];

                    if (empty($v) && !$bypassEmpty) {
                        $formFieldsDataFiltered[$field] = null;
                    } else {
                        $formFieldsDataFiltered[$field] = $v;
                    }
                }
            }
        }

        return $this->load([$formName => $formFieldsDataFiltered]);
    }

    public function addExtraData($name, $value)
    {
        $this->extraData[$name] = $value;
    }

    public function addExtraMessage($message)
    {
        $this->extraMessages[] = $message;
    }

    public function addExtraWarning($warning)
    {
        $this->extraWarnings[] = $warning;
    }

    public function addExtraWarningPassthrough($warning)
    {
        $this->extraWarningsPassthrough[] = $warning;
    }

    public function addExtraError($error)
    {
        $this->extraErrors[] = $error;
    }

    public function addExtraErrorPassthrough($error)
    {
        $this->extraErrorsPassthrough[] = $error;
    }

    public function addExtraException($e)
    {
        if (is_string($e)) {
            $this->extraExceptions[] = $e;
        } else {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            $this->extraExceptions[] = $msg;
        }
    }

    public function addExtraExceptionPassthrough($e)
    {
        if (is_string($e)) {
            $this->extraExceptionsPassthrough[] = $e;
        } else {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            $this->extraExceptionsPassthrough[] = $msg;
        }
    }

    /**
     * @return bool
     */
    public function hasErrorsExtra()
    {
        return $this->hasErrors() || count($this->extraWarnings) > 0 || count($this->extraErrors) > 0 || count($this->extraExceptions) > 0;
    }

    /**
     * @return bool
     */
    public function hasErrorsOrPassthroughExtra()
    {
        return $this->hasErrors() || count($this->extraWarnings) > 0 || count($this->extraErrors) > 0 || count($this->extraExceptions) > 0 ||
        count($this->extraWarningsPassthrough) > 0 || count($this->extraErrorsPassthrough) > 0 || count($this->extraExceptionsPassthrough) > 0;
    }

    public function buildResult($result = ['_extra' => ['ok' => true]])
    {
        $_extra = &$result['_extra'];
        $_extra['data'] = $this->extraData;
        $_extra['messages'] = $this->extraMessages;
        $_extra['warnings'] = array_merge($this->extraWarningsPassthrough, $this->extraWarnings);
        $_extra['errors'] = array_merge($this->extraErrorsPassthrough, $this->extraErrors);
        $_extra['exceptions'] = array_merge($this->extraExceptionsPassthrough, $this->extraExceptions);

        if ($this->hasErrorsExtra()) {
            $_extra['ok'] = false;

            if ($this->hasErrors()) {
                foreach ($this->getErrors() as $attribute => $errors) {
                    $result[Html::getInputId($this, $attribute)] = $errors;
                }
            }
        }

        return $result;
    }

    public function buildAjaxResult($result = ['_extra' => ['ok' => true]])
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->buildResult($result);
    }
}
