<?php
namespace bdb\component;
define("None", "");

class InternalServiceIntegration
{
    //private $addr = "127.0.0.1:9971";
    private $addr = "pip.mobi:9971";
    private $internalKey = "df07098dfhklj098089sdpfhklhlkh";
    var $lastError = null;

    private function request($url, $content = None, $onError = None, $onResult = None, $onProgress = None)
    {
        $lastError = null;
        $buff = "";
        $mode = "";

        $callback = function ($curlHandle, $data) use (&$buff, &$mode, &$onProgress) {
            $dataLen = strlen($data);

            if (!$onProgress) {
                $buff .= $data;
            } else {
                for ($i = 0; $i < $dataLen; $i++) {
                    $c = $data[$i];

                    if ($c == "\n" && $mode != "#2") {
                        if ($buff == "#1" || $buff == "#2") {
                            $mode = $buff;
                        } else if ($mode == "#1") {
                            $onProgress($buff);
                        }

                        $buff = "";
                    } else {
                        $buff .= $c;
                    }
                }
            }

            return $dataLen;
        };

        $contentLen = strlen($content);
        $curlHandle = curl_init();

        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, [
            "Connection: close",
            "X-Connection: close",
            "Content-Type: text/plain",
            "Content-Length: $contentLen",
            "X-PIP-INTERNAL: $this->internalKey"
        ]);

        curl_setopt_array($curlHandle, [
            CURLOPT_URL => $url,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_USERAGENT => 'PIP-BACKEND',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 120,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_BUFFERSIZE => 256,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $content,
            CURLOPT_WRITEFUNCTION => $callback,
            CURLOPT_FAILONERROR => true
        ]);

        $error = false;
        curl_exec($curlHandle) or $error = curl_error($curlHandle);
        $code = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
        curl_close($curlHandle);
        $result = null;

        if ($code == 200) {
            if ($error) {
                $result = false;
                $this->lastError = $error;

                if ($onError) {
                    $onError(0, $error);
                }
            } else {
                $result = $buff;
                $lastError = null;

                if ($onResult) {
                    $onResult($buff);
                }
            }
        } else {
            $result = false;
            $this->lastError = $error;

            if ($onError != null) {
                $onError($code, "RemoteException");
            }
        }

        return $result;
    }

    function tsid()
    {
        $url = "https://$this->addr/webservice/Util/tsid";
        return $this->request($url);
    }

    function tsuuid()
    {
        $url = "https://$this->addr/webservice/Util/tsuuid";
        return $this->request($url);
    }

    function kid($id, $baseLenght = 7)
    {
        $url = "https://$this->addr/webservice/Util/kid/$id/$baseLenght";
        return $this->request($url);
    }

    function activationKey($id, $baseLenght = 7)
    {
        $url = "https://$this->addr/webservice/Util/kid/$id/$baseLenght";
        return $this->request($url);
    }

    function generateTicketDump($authAccountInternalAccessKey, $content, $onProgress = None)
    {
        $url = "https://$this->addr/webservice/TicketDump/generate/$authAccountInternalAccessKey";
        return $this->request($url, $content, None, None, $onProgress);
    }

    function generateEventKeys($authAccountInternalAccessKey, $eventTsuuid)
    {
        $url = "https://$this->addr/webservice/EventRelated/generateEventKeys/$authAccountInternalAccessKey/$eventTsuuid";
        return $this->request($url);
    }

    function getOrCreateSolutionUser($authAccountInternalAccessKey, $email)
    {
        $encodedEmail = urlencode($email);
        $url = "https://$this->addr/webservice/SolutionUser/getOrCreate/$authAccountInternalAccessKey/$encodedEmail";
        return $this->request($url);
    }

    function transferTicket($authAccountInternalAccessKey, $ticketTsuuid, $email)
    {
        $content = json_encode([
            "ticketTsuuid" => $ticketTsuuid,
            "email" => $email
        ]);

        $url = "https://$this->addr/webservice/TicketTransfer/transfer/$authAccountInternalAccessKey";
        return $this->request($url, $content);
    }

    function transferTicketFinish($authAccountInternalAccessKey, $ticketTransferTsuuid)
    {
        $content = json_encode([
            "ticketTransferTsuuid" => $ticketTransferTsuuid
        ]);

        $url = "https://$this->addr/webservice/TicketTransfer/transferFinish/$authAccountInternalAccessKey";
        return $this->request($url, $content);
    }

    function sendEmailAndPush($authAccountInternalAccessKey, $authAccountTsuuid, $group,
                              $emailTitle, $emailContent, $pushTitle, $pushContent, $source, $sourceKind, $sourceId)
    {
        $content = json_encode([
            "authAccountTsuuid" => $authAccountTsuuid,
            "group" => $group,
            "emailTitle" => $emailTitle,
            "emailContent" => $emailContent,
            "pushTitle" => $pushTitle,
            "pushContent" => $pushContent,
            "source" => $source,
            "sourceKind" => $sourceKind,
            "sourceId" => $sourceId
        ]);

        $url = "https://$this->addr/webservice/AuthAccount/sendEmailAndPush/$authAccountInternalAccessKey";
        return $this->request($url, $content);
    }

    function updateEventLot($authAccountInternalAccessKey, $eventLotTsuuid, $params)
    {
        $content = json_encode([
            "tsuuid" => $eventLotTsuuid,
            "params" => $params
        ]);

        $url = "https://$this->addr/webservice/EventRelated/updateEventLot/$authAccountInternalAccessKey";
        return $this->request($url, $content);
    }

    function deleteEventLot($authAccountInternalAccessKey, $eventLotTsuuid)
    {
        $url = "https://$this->addr/webservice/EventRelated/deleteEventLot/$authAccountInternalAccessKey/$eventLotTsuuid";
        return $this->request($url);
    }
}

