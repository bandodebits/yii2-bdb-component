<?php
namespace bdb\component;

use yii\base\InvalidConfigException;
use yii\validators\Validator;

trait ModelTrait
{
    public function rules()
    {
        return $this::modelRules();
    }

    public static function modelRules()
    {
        return [];
    }

    public function attributeLabels()
    {
        return $this::modelAttributeLabels();
    }

    public static function internalAttributes()
    {
        return [];
    }

    public static function modelAttributeLabels()
    {
        return [];
    }

    public static function modelAttributes()
    {
        //Late static binding, http://php.net/manual/en/language.oop5.late-static-bindings.php
        $rules = static::modelRules();
        $result = [];

        foreach ($rules as $rule) {
            if ($rule instanceof Validator) {
                $attributes = $rule->attributes;

                if (is_array($attributes)) {
                    foreach ($attributes as $attribute) {
                        $result[$attribute] = 0;
                    }
                } else {
                    $result[$attributes] = 0;
                }
            } elseif (is_array($rule) && isset($rule[0], $rule[1])) { // attributes, validator type
                foreach ((array)$rule[0] as $attribute) {
                    $result[$attribute] = 0;
                }
            } else {
                throw new InvalidConfigException('Invalid validation rule: a rule must specify both attribute names and validator type.');
            }
        }

        return array_keys($result);
    }
}