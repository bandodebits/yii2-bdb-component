<?php
namespace bdb\component;

use bdb\component\lang\String;

class Util
{
    public static function camelCaseToUrl($value)
    {
        $strArr = str_split($value);
        $result = strtolower($strArr[0]);

        for ($i = 1; $i < count($strArr); $i++) {
            $c = $strArr[$i];

            if (ctype_upper($c)) {
                $result .= "-";
            }

            $result .= strtolower($c);
        }

        return $result;
    }

    public static function isTsuuid($value)
    {
        return !empty($value) && strlen($value) == 54;
    }

    public static function sqlSafe($value)
    {
        return str_replace("'", " ", $value);
    }

    public static function startsWith($underlying, $fragment)
    {
        return String::startsWith($underlying, $fragment);
    }

    public static function endsWith($underlying, $fragment)
    {
        return String::endsWith($underlying, $fragment);
    }
}