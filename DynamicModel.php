<?php
namespace bdb\component;

use yii\base\InvalidCallException;
use yii\base\UnknownMethodException;
use yii\base\UnknownPropertyException;

class DynamicModel extends \yii\base\DynamicModel
{
    use ModelTrait;

    public $delegates = null;

    /**
     * @param array $attributes
     * @param array $config
     * @param array $delegates
     * @throws \yii\base\InvalidConfigException
     */
    public function __construct($attributes = [], $config = [], $delegates = [])
    {
        $this->delegates = $delegates;
        parent::__construct(array_merge($this::modelAttributes(), $attributes), $config);
    }

    public function addDelegate($delegate)
    {
        $this->delegates[] = $delegate;
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if (empty($this->delegates)) {
            return parent::__get($name);
        } else {
            try {
                return parent::__get($name);
            } catch (UnknownPropertyException $e) {
                foreach ($this->delegates as $delegate) {
                    try {
                        return $delegate->$name;
                    } catch (UnknownPropertyException $e) {
                        //Silent
                    }
                }

                throw new UnknownPropertyException('Getting unknown property: ' . get_class($this) . '::' . $name);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        if (empty($this->delegates)) {
            parent::__set($name, $value);
        } else {
            try {
                parent::__set($name, $value);
            } catch (UnknownPropertyException $e) {
                foreach ($this->delegates as $delegate) {
                    try {
                        $delegate->$name = $value;
                        return;
                    } catch (UnknownPropertyException $e) {
                        //Silent
                    }
                }

                throw new UnknownPropertyException('Setting unknown property: ' . get_class($this) . '::' . $name);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function __isset($name)
    {
        if (empty($this->delegates)) {
            return parent::__isset($name);
        } else {
            if (parent::__isset($name)) {
                return true;
            } else {
                foreach ($this->delegates as $delegate) {
                    if (isset($delegate->$name)) {
                        return true;
                    }
                }

                return false;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function __unset($name)
    {
        if (empty($this->delegates)) {
            parent::__unset($name);
        } else {
            try {
                parent::__unset($name);
            } catch (InvalidCallException $e) {
                foreach ($this->delegates as $delegate) {
                    try {
                        unset($delegate->$name);
                    } catch (UnknownPropertyException $e) {
                        //Silent
                    }
                }

                throw new InvalidCallException('Unsetting an unknown or read-only property: ' . get_class($this) . '::' . $name);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function __call($name, $params)
    {
        if (empty($this->delegates)) {
            return parent::__call($name, $params);
        } else {
            try {
                return parent::__call($name, $params);
            } catch (UnknownMethodException $e) {
                foreach ($this->delegates as $delegate) {
                    try {
                        return call_user_func_array([$delegate, $name], $params);
                    } catch (UnknownMethodException $e) {
                        //Silent
                    }
                }

                throw new UnknownMethodException('Calling unknown method: ' . get_class($this) . "::$name()");
            }
        }
    }
}
